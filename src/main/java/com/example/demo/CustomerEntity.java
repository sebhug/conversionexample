package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEntity {
    private Long id;
    private UUID objectId;
    private String name;
    private Timestamp birthday;
    private Timestamp insDate;
    private Timestamp updDate;
    private Long version;
}
