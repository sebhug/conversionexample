package com.example.demo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class CustomerToEntity implements Converter<CustomerDto, CustomerEntity> {
    @Override
    public CustomerEntity convert(CustomerDto source) {
        // ModelMapper mapper = new ModelMapper();
        // CustomerEntity = mapper.map(source, CustomerEntity.class);
        return CustomerEntity.builder()
                .objectId(source.getId())
                .name(source.getName())
                .birthday(Timestamp.from(source.getBirthday()))
                .build();
    }
}
