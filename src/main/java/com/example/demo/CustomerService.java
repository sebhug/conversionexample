package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private final ConversionService conversionService;

    @Autowired
    public CustomerService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    public void createCustomer(CustomerDto customerDto) {
        CustomerEntity entity = conversionService.convert(customerDto, CustomerEntity.class);
    }

}
